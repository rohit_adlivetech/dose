<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/emails/preview/{id}', 'EmailController@preview');
Route::get('/subscribers/unsubscribe/{token}', 'SubscriberController@unsubscribe');

Route::group(['middleware' => ['auth', 'auth.admin']], function() {
	Route::get('/','EmailController@index');
	Route::get('/emails/new', function () { return view('email.edit'); });
	Route::post('/emails/new', 'EmailController@save');
	Route::get('/emails', 'EmailController@index');
	//Route::get('/emails/preview/{id}', 'EmailController@preview');
	Route::post('/emails/delete/{id}', 'EmailController@delete');	
	Route::get('/emails/{id}', 'EmailController@get');
	Route::post('/emails/{id}', 'EmailController@update');

	Route::post('/furtherReading/edit/{id}', 'FurtherReadingController@update');	
	Route::post('/furtherReading/delete/{id}', 'FurtherReadingController@delete');
	Route::get('/furtherReading/{id}', 'FurtherReadingController@index');	
	Route::post('/furtherReading/{id}', 'FurtherReadingController@save');

	Route::get('/subscribers', 'SubscriberController@index');
	Route::get('/subscribers/new', function () { return view('subscriber.edit'); });
	Route::post('/subscribers/new', 'SubscriberController@save');
	Route::get('/subscribers/{id}', 'SubscriberController@get');
	Route::post('/subscribers/{id}', 'SubscriberController@update');
	
	// Route::get('/home', 'HomeController@index');
	//Route::get('/adduser', 'UserController@index');
});

Auth::routes();

