@extends('layouts.dashboard')

@section('content')

	
	<!-- content -->
	<div id="content" class="app-content box-shadow-z3" role="main">
		<div class="app-header white box-shadow">
				<div class="navbar navbar-toggleable-sm flex-row align-items-center">
						<!-- Open side - Naviation on mobile -->
						<a data-toggle="modal" data-target="#aside" class="hidden-lg-up mr-3">
							<i class="material-icons">&#xe5d2;</i>
						</a>
						<!-- / -->
				
						<!-- Page title - Bind to $state's title -->
						<div class="mb-0 h5 no-wrap" ng-bind="$state.current.data.title" id="pageTitle"></div>
				
						<!-- navbar collapse -->
						<div class="collapse navbar-collapse" id="collapse">
							<!-- link and dropdown -->
							<ul class="nav navbar-nav mr-auto">
								<li class="nav-item dropdown">
									<a class="nav-link" href="/emails/new">
										<i class="fa fa-fw fa-plus text-muted"></i>
										<span>New</span>
									</a>
									<div ui-include="'../views/blocks/dropdown.new.html'"></div>
								</li>
							</ul>
				
							
							<!-- / -->
						</div>
						<!-- / navbar collapse -->
				
						<!-- navbar right -->
				
						<!-- / navbar right -->
				</div>
		</div>   
		<div ui-view class="app-body" id="view">
			
<!-- ############ PAGE START-->
<div class="dker">
	<div class="tab-content tab-alt">
		<div class="tab-pane in active" id="world">
			<div class="padding">
				<div class="row-col">
					<div class="box">
						<div class="table-responsive">
							<table ui-jp="dataTable" ui-options="{'pageLength':100}" class="table table-striped b-t b-b">
								<thead>
									<tr>
										<th  style="width:20%">Email Subject</th>																
										<th  style="width:20%">Action</th>								
									</tr>
								</thead>
								<tbody>
									@foreach($emails as $email)
										<tr>
											<td> {{ $email->subject }} </td>
											<td>
												<a href="/emails/{{ $email->id }}" class="btn btn-default">Edit</a>
												<!-- <a href="#" class="btn btn-default">Delete</a> -->
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			
		</div>
		
	</div>
	<div class="row-col">
		
		
	</div>
</div>
<div class="row no-gutter">
	
</div>
<div class="row no-gutter">
	
</div>
@endsection
