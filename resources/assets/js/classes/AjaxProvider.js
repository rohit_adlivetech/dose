var axios = require('axios');
export class AjaxProvider {
	constructor(props) {
		this.http = axios		
	}

	getFurtherReadingsForEmail( emailId ) {
		return axios.get('/furtherReading/' + emailId )  		
	}

	updateFurtherReadingById( id, data ) {
		return axios.post('/furtherReading/edit/' + id , data)
	}

	deleteItem( url ) {
		return axios.post( url )
	}
}