

// Vue.component('example', require('./components/Example.vue'));
window.axios = require('axios');
window.axios.defaults.headers.common = {
    'X-CSRF-TOKEN': window.Laravel.csrfToken,
    'X-Requested-With': 'XMLHttpRequest'
};

window.Vue = require('vue');
// window.$ = window.jQuery = require('jquery');


Vue.component('example', require('./components/Example.vue'));
Vue.component('furtherReading', require('./components/furtherReading.vue'));
Vue.component('summerNote', require('./components/summerNote.vue'));

const app = new Vue({
    el: '#app',
    mounted() {

    }
});

window.addEventListener('load', function() {
	window.$('.summernote').summernote({
	  // airMode: true
	  toolbar: [
	      // [groupName, [list of button]]
	      ['style', ['bold', 'italic', 'underline', 'clear']],
	      ['font', ['strikethrough', 'superscript', 'subscript']],
	      ['fontsize', ['fontsize']],
	      ['color', ['color']],
	      ['para', ['ul', 'ol', 'paragraph']],
	      ['height', ['height']]
	    ]
	});

})

