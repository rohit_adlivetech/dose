<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    //
     public function furtherReading()
    {
        return $this->hasMany('App\FurtherReading');
    }
}
