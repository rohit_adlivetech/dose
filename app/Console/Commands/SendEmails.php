<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Mail\DailyQuote;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Subscriber;
use App\Email;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'newsletter:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }



    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $subscribers = Subscriber::all();

        $email = Email::with('furtherReading')
        ->whereDate('last_sent', '<=', \Carbon\Carbon::today()->subDays(60) )
        ->orWhere('last_sent', '=', null )
        ->get()
        ->random();

        $subscribers->each( function( $subscriber ) use ( $email ) {
            Mail::to( $subscriber->email )->send( new DailyQuote( $email, $subscriber ) );
        });         

        $email->last_sent = \Carbon\Carbon::now()->toDateTimeString();
        $email->save();
    }
}
