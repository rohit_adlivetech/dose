<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subscriber;

class SubscriberController extends Controller
{
    //
    public function index()
    {
        $subscribers = Subscriber::all();
        return view('subscriber.index', ['subscribers' => $subscribers ]);
    }

    public function get( $id )
    {
        $subscriber = Subscriber::find( $id );
        return view('subscriber.edit', ['subscriber' => $subscriber]);
    }

    public function save( Request $request )
    {
        $subscriber = new Subscriber();
        $subscriber->email = $request->get('email');     
        $subscriber->remember_token = str_random(10);
        $subscriber->save();
        return redirect('/subscribers');
    }

    public function update( Request $request, $id )
    {
        $subscriber = Subscriber::find( $id );
        $subscriber->email = $request->get('email');       
        $subscriber->save();
        return redirect('/subscribers');
    }

    public function unsubscribe(Request $request, $token) {
        $subscriber = Subscriber::where( 'remember_token', '=', $token )->first();
        $subscriber->delete();
        return view('email.unsubscribe');
    }

    public function delete()
    {

    }
}
