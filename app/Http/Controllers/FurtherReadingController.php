<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FurtherReading;

class FurtherReadingController extends Controller
{
    //
    public function index( $id )
    {
        
        $furtherReading = FurtherReading::where('email_id', '=', $id )->get();        
        return response()->json([
            'code' => 200,
            'data' => $furtherReading
        ]);
    }

    public function save( Request $request )
    {
        $furtherReading = new FurtherReading();
        if( isset( $request->file ) ) {            
            $path = $request->file->store('public/thumbnails');  
            $furtherReading->thumb_path = $path;
        }
        $furtherReading->thumb_path = $path;
        $furtherReading->email_id = $request->get('email_id');
        $furtherReading->link = $request->get('link');
        $furtherReading->title = $request->get('title');
        $furtherReading->copy = $request->get('copy');
        $furtherReading->save();
    }

    public function update( Request $request, $id )
    {
        $furtherReading = FurtherReading::find( $id );
        if( isset( $request->file ) ) {            
            $path = $request->file->store('public/thumbnails');  
            $furtherReading->thumb_path = $path;
        }
        
        $furtherReading->email_id = $request->get('email_id');
        $furtherReading->link = $request->get('link');
        $furtherReading->title = $request->get('title');
        $furtherReading->copy = $request->get('copy');
        $furtherReading->save();
        return response()->json([
            'code' => 200,
            'data' => $furtherReading
        ]);
    }

    public function delete( $id )
    {
        $furtherReading = FurtherReading::find( $id );
        $furtherReading->delete();
    }
}
