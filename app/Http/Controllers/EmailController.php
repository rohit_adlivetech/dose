<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Email;

class EmailController extends Controller
{
    //
    public function index()
    {
    	$emails = Email::all();
        return view('email.index', [ 'emails' => $emails ]);
    }

    public function get( $id )
    {
        $email = Email::find( $id );
        return view('email.edit', [ 'email' => $email ]);
    }    

    public function save( Request $request )
    {
    	$email = new Email();
    	$email->subject = $request->get('subject');
    	$email->body = $request->get('body');
    	$email->quote = $request->get('quote');
        $email->quote_author = $request->get('quote_author');
    	$email->last_sent = \Carbon\Carbon::now()->subWeek(2)->toDateTimeString();
    	$email->save();
        return redirect('/emails/' . $email->id );
        //return back();
    }

    public function update( Request $request, $id )
    {
    	$email = Email::find( $id );
    	$email->subject = $request->get('subject');
    	$email->body = $request->get('body');
    	$email->quote = $request->get('quote');
        $email->quote_author = $request->get('quote_author');
    	//$email->last_sent = $request->get('last_sent');
    	$email->save();
        return redirect('/emails');
    }

    public function delete( $id )
    {
    	$email = Email::with('furtherReading')->find( $id );    	
        $email->furtherReading->each( function( $item ){
            $item->delete();
        });
    	$email->delete();
        return redirect('/emails');
    }

    public function preview( $id )
    {
        $email = Email::with('furtherReading')->find( $id );    
        return view('email_template.email', [ 'email' => $email ]);
    }
}
