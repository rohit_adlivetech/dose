<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class FurtherReading extends Model
{
	protected $table = 'FurtherReadings';
    //
     public function email()
    {
        return $this->belongsTo('App\Email');
    }
}
