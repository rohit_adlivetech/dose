<?php

namespace App\Mail;

use App\Email;
use App\Subscriber;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DailyQuote extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $email;

    public $user;

    public function __construct( Email $email, Subscriber $user )
    {
        //
        $this->email = $email;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject( $this->email->subject . ' - ' . \Carbon\Carbon::today()->format('F d') )
        ->from('daily@doseofpower.com', 'Daily Dose of Power')
        ->view('email_template.email');
    }
}
