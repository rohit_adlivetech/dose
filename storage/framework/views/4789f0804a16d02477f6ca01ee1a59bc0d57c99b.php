<?php $__env->startSection('content'); ?>

  
  <!-- content -->
  <div id="content" class="app-content box-shadow-z3" role="main">
    <div class="app-header white box-shadow">
        <div class="navbar navbar-toggleable-sm flex-row align-items-center">
            <!-- Open side - Naviation on mobile -->
            <a data-toggle="modal" data-target="#aside" class="hidden-lg-up mr-3">
              <i class="material-icons">&#xe5d2;</i>
            </a>
            <!-- / -->
        
            <!-- navbar collapse -->
            <div class="collapse navbar-collapse" id="collapse">
              <!-- link and dropdown -->
              <ul class="nav navbar-nav mr-auto">
                <li class="nav-item dropdown">
                  <a class="nav-link" href="/subscribers">
                    <i class="fa fa-fw fa-reply text-muted"></i>
                    <span>View All</span>
                  </a>
                  <div ui-include="'../views/blocks/dropdown.new.html'"></div>
                </li>
              </ul>
        
              <div ui-include="'../views/blocks/navbar.form.html'"></div>
              <!-- / -->
            </div>
            <!-- / navbar collapse -->
        
            <!-- navbar right -->
   
            <!-- / navbar right -->
        </div>
    </div>   
    <div ui-view class="app-body" id="view">
      
<!-- ############ PAGE START-->
<div class="dker">
  <div class="tab-content tab-alt">
    <div class="tab-pane in active" id="world">
      <div class="padding">
        <div class="row-col">
          <div class="box">
           <div class="col-md-12">
                 <div class="box">
                   <div class="box-header">
                     <h2>New Email Template </h2>
                     <small>Enter data used to populate an email here.</small>
                   </div>
                   <div class="box-divider m-0"></div>
                   <div class="box-body">
                   <?php if( isset($subscriber) ): ?>
                   <form role="form" action="/subscribers/<?php echo e($subscriber->id); ?>" method="POST">
                   <?php else: ?>
                   <form role="form" action="/subscribers/new" method="POST">
                   <?php endif; ?>
                        <?php echo e(csrf_field()); ?>

                        <div class="form-group row">
                         <label for="inputEmail3" class="col-sm-2 form-control-label">Email:</label>
                         <div class="col-sm-10">
                         <?php if( isset($subscriber) ): ?>
                           <input name="email" type="text" class="form-control" id="inputEmail3" value="<?php echo e($subscriber->email); ?>" placeholder="Email">
                         <?php else: ?>
                           <input name="email" type="text" class="form-control" id="inputEmail3" placeholder="Email">
                         <?php endif; ?>  
                         </div>
                       </div>                                                                         
                       <div class="form-group row m-t-md">
                         <div class="col-sm-offset-2 col-sm-10">
                           <?php if( isset($subscriber) ): ?>
                             <button type="submit" class="btn white">Edit</button>
                           <?php else: ?>
                           <button type="submit" class="btn white">Save</button>
                           <?php endif; ?>
                         </div>
                       </div>
                     </form>                                   
                   </div>
                 </div>
               </div>
          </div>
        </div>
      </div>
      
    </div>
    
  </div>
  <div class="row-col">
    
    
  </div>
</div>
<div class="row no-gutter">
  
</div>
<div class="row no-gutter">
  
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>