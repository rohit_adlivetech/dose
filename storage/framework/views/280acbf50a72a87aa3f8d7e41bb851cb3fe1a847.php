<?php $__env->startSection('content'); ?>

  
  <!-- content -->
  <div id="content" class="app-content box-shadow-z3" role="main">
    <div class="app-header white box-shadow">
        <div class="navbar navbar-toggleable-sm flex-row align-items-center">
            <!-- Open side - Naviation on mobile -->
            <a data-toggle="modal" data-target="#aside" class="hidden-lg-up mr-3">
              <i class="material-icons">&#xe5d2;</i>
            </a>
            <!-- / -->
        
            <!-- navbar collapse -->
            <div class="collapse navbar-collapse" id="collapse">
              <!-- link and dropdown -->
              <ul class="nav navbar-nav mr-auto">
                <li class="nav-item dropdown">
                  <a class="nav-link" href="/emails">
                    <i class="fa fa-fw fa-reply text-muted"></i>
                    <span>View All</span>
                  </a>
                  <div ui-include="'../views/blocks/dropdown.new.html'"></div>
                </li>
              </ul>
        
              <div ui-include="'../views/blocks/navbar.form.html'"></div>
              <!-- / -->
            </div>
            <!-- / navbar collapse -->
        
            <!-- navbar right -->
           
            <!-- / navbar right -->
        </div>
    </div>   
    <div ui-view class="app-body" id="view">
      
<!-- ############ PAGE START-->
<div class="dker">
  <div class="tab-content tab-alt">
    <div class="tab-pane in active" id="world">
      <div class="padding">
        <div class="row-col">
          <div class="box">
          
           <div class="col-md-12">
                 <div class="box">
                   <div class="box-header">
                     <h2>New Email Template </h2>
                     <small>Enter data used to populate an email here.</small>
                     <?php if( isset($email->id) ): ?>
                     <div class="form-group">
                        <button class="btn danger pull-right" data-toggle="modal" data-target="#m-sm">Delete</button>
                        <a class="btn primary pull-right" target="_blank" href="/emails/preview/<?php echo e($email->id); ?>">Preview</a>
                        <div style="clear:both;"></div>
                      </div>                                             
                      <?php endif; ?>
                   </div>
                   <div class="box-divider m-0 clearfix"></div>
                   <div class="box-body">
                   <?php if( isset($email) ): ?>
                   <form role="form" action="/emails/<?php echo e($email->id); ?>" method="POST">
                        <?php echo e(csrf_field()); ?>

                       <div class="form-group row">
                         <label for="inputEmail3" class="col-sm-2 form-control-label">Subject</label>
                         <div class="col-sm-10">
                           <input name="subject" type="text" class="form-control" id="inputEmail3" value="<?php echo e($email->subject); ?>" >
                         </div>
                       </div>
                       <div class="form-group row">
                         <label for="inputPassword3" class="col-sm-2 form-control-label"> Body</label>
                         <div class="col-sm-10" >
                           <textarea name="body" class="form-control summernote" rows="2"><?php echo e($email->body); ?></textarea>
                         </div>
                         
                       </div>
                       <div class="form-group row">
                         <label for="inputPassword3" class="col-sm-2 form-control-label">Quote</label>
                         <div class="col-sm-10">
                           <textarea name="quote" class="form-control" rows="2"><?php echo e($email->quote); ?></textarea>
                         </div>
                       </div>
                       <div class="form-group row">
                         <label for="inputEmail3" class="col-sm-2 form-control-label">Quote Author</label>
                         <div class="col-sm-10">
                           <input name="quote_author" type="text" class="form-control" id="inputEmail3" value="<?php echo e($email->quote_author); ?>" >
                         </div>
                       </div>
                                                                         
                       <div class="form-group row m-t-md">
                         <div class="col-sm-offset-2 col-sm-10">
                           <button type="submit" class="btn white">Update</button>
                         </div>
                       </div>
                     </form>
                   <?php else: ?> 
                    <form role="form" action="/emails/new" method="POST">
                       <?php echo e(csrf_field()); ?>

                      <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 form-control-label">Subject</label>
                        <div class="col-sm-10">
                          <input name="subject" type="text" class="form-control" id="inputEmail3" placeholder="Email">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-2 form-control-label">Body</label>
                        <div class="col-sm-10">
                          <textarea name="body" class="form-control" rows="2"></textarea>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-2 form-control-label">Quote</label>
                        <div class="col-sm-10">
                          <textarea name="quote" class="form-control" rows="2"></textarea>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 form-control-label">Quote Author</label>
                        <div class="col-sm-10">
                          <input name="quote_author" type="text" class="form-control" id="inputEmail3"  >
                        </div>
                      </div>
                                                                        
                      <div class="form-group row m-t-md">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn white">Submit</button>
                        </div>
                      </div>
                    </form>
                   <?php endif; ?>
                     
                   </div>
                 </div>
               </div>
          </div>
        </div>
      </div>
      
    </div>
    
  </div>
  <?php if( isset($email) ): ?>
  <div class="row-col">
      <div class="tab-content tab-alt">
        <div class="tab-pane in active" id="world">
          <div class="padding">
             <div class="box">
                <div class="col-md-12">
                <further-reading email-id="<?php echo e($email->id); ?>"></further-reading>
                 </div>
              </div>
          </div>          
        </div>        
      </div>
    
  </div>
  <?php endif; ?>

</div>
<!-- small modal -->
<?php if( isset($email) ): ?>
<div id="m-sm" class="modal" data-backdrop="true">
  <div class="row-col h-v">
    <div class="row-cell v-m">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Delete User</h5>
          </div>
          <div class="modal-body text-center p-lg">
            <p>Are you sure to execute this action?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn white p-x-md" data-dismiss="modal">No</button>
            <form action="/emails/delete/<?php echo e($email->id); ?>" method="post">
               <?php echo e(csrf_field()); ?>

               <button class="btn danger" type="submit">Yes - Delete email</button>
             </form>
            
            
          </div>
        </div><!-- /.modal-content -->
      </div>
    </div>
  </div>
</div>
<?php endif; ?>
<!-- / .modal -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script src="<?php echo e(asset('js/app.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>