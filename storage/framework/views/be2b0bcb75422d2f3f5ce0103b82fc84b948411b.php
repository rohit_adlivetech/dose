<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>Daily Dose of Power</title>
  <meta name="description" content="Admin, Dashboard, Bootstrap, Bootstrap 4, Angular, AngularJS" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- for ios 7 style, multi-resolution icon of 152x152 -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
  <link rel="apple-touch-icon" href="/assets/images/logo.png">
  <meta name="apple-mobile-web-app-title" content="Flatkit">
  <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
  <meta name="mobile-web-app-capable" content="yes">
  <link rel="shortcut icon" sizes="196x196" href="/assets/images/logo.png">
  
  <!-- style -->
  <link rel="stylesheet" href="/assets/animate.css/animate.min.css" type="text/css" />
  <link rel="stylesheet" href="/assets/glyphicons/glyphicons.css" type="text/css" />
  <link rel="stylesheet" href="/summernote/summernote.css" type="text/css" />
  
  <link rel="stylesheet" href="/assets/font-awesome/css/font-awesome.min.css" type="text/css" />
  <link rel="stylesheet" href="/assets/material-design-icons/material-design-icons.css" type="text/css" />

  <link rel="stylesheet" href="/assets/bootstrap/dist/css/bootstrap.min.css" type="text/css" />
  <!-- build:css /assets/styles/app.min.css -->
  <link rel="stylesheet" href="/assets/styles/app.css" type="text/css" />
  <!-- endbuild -->
  <link rel="stylesheet" href="/assets/styles/font.css" type="text/css" />
  <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>;
    </script>
    <style>
      .form-group.row {
        margin: 30px 0 ;
      }
      .note-popover {display: none !important;}
      .note-editor.note-frame .note-editing-area .note-editable {          
          border: 1px solid #ecebeb;
      }

    </style>
</head>
<body>
  <div class="app" id="app">

<!-- ############ LAYOUT START-->
<!-- aside -->
  <div id="aside" class="app-aside modal fade folded md show-text nav-dropdown">
    <div class="left navside black dk" layout="column">
      <div class="navbar no-radius">
        <!-- brand -->
        <a class="navbar-brand">
            <!-- <div ui-include="'../assets/images/logo.svg'"></div> -->
            <!-- <img src="../assets/images/logo.png" alt="." class="hide"> -->
            <span class="hidden-folded inline" style="font-size: 14px;">Daily Dose of Power</span>
        </a>
        <!-- / brand -->
      </div>
      <div flex class="hide-scroll">
          <nav class="scroll nav-active-primary">
            
              <ul class="nav" ui-nav>
                <li class="nav-header hidden-folded">
                  <small class="text-muted">Main</small>
                </li>
                
                <li>
                  <a href="/emails" >
                    <span class="nav-icon">
                      <i class="material-icons">&#xe3fc;
                        <span ui-include="'../assets/images/i_0.svg'"></span>
                      </i>
                    </span>
                    <span class="nav-text">Emails</span>
                  </a>
                </li>
                <li>
                  <a href="/subscribers" >
                    <span class="nav-icon">
                      <i class="material-icons">&#xe3fc;
                        <span ui-include="'../assets/images/i_0.svg'"></span>
                      </i>
                    </span>
                    <span class="nav-text">Subscribers</span>
                  </a>
                </li>                                            
              </ul>
          </nav>
      </div>
     
    </div>
  </div>
  <!-- / aside -->
   <?php echo $__env->yieldContent('content'); ?>
  

<!-- ############ PAGE END-->

    </div>
  </div>
  <!-- / -->

  <!-- theme switcher -->
  
  <!-- / -->

<!-- ############ LAYOUT END-->

  </div>
<!-- build:js /scripts/app.html.js -->
<!-- jQuery -->
  <script src="/libs/jquery/jquery/dist/jquery.js"></script>
<!-- Bootstrap -->
  <script src="/libs/jquery/tether/dist/js/tether.min.js"></script>
  <script src="/libs/jquery/bootstrap/dist/js/bootstrap.js"></script>
<!-- core -->
  <script src="/libs/jquery/underscore/underscore-min.js"></script>
  <script src="/libs/jquery/jQuery-Storage-API/jquery.storageapi.min.js"></script>
  <script src="/libs/jquery/PACE/pace.min.js"></script>

  <script src="/scripts/config.lazyload.js"></script>

  <script src="/scripts/palette.js"></script>
  <script src="/summernote/summernote.js"></script>
  <script src="/scripts/ui-load.js"></script>
  <script src="/scripts/ui-jp.js"></script>
  <script src="/scripts/ui-include.js"></script>
  <script src="/scripts/ui-device.js"></script>
  <script src="/scripts/ui-form.js"></script>
  <script src="/scripts/ui-nav.js"></script>
  <script src="/scripts/ui-screenfull.js"></script>
  <script src="/scripts/ui-scroll-to.js"></script>
  <script src="/scripts/ui-toggle-class.js"></script>

  <script src="/scripts/app.js"></script>
  <!-- ajax -->
  <!-- <script src="/libs/jquery/jquery-pjax/jquery.pjax.js"></script> -->
  <script src="/scripts/ajax.js"></script>
  
     <?php echo $__env->yieldContent('scripts'); ?>
<!-- endbuild -->
</body>
</html>
